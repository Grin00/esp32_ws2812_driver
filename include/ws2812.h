/*
 * File: 	ws2812.h
 * Author: 	Alexander Grin, Dnipro, Ukraine
 * Emain: 	gryn2000@gmail.com
 */
#ifndef WS2812_DRIVER_H
#define WS2812_DRIVER_H
#include <stdint.h>
#include "esp_err.h"

typedef enum
{
    WS2812_RGB = 0,
    WS2812_GRB
}ws2812_leds_type_t;

typedef struct
{
	uint32_t            gpio_pin;
	int                 leds_count;
	ws2812_leds_type_t  leds_type;
	uint8_t             brightness;
}ws2812_t;

/**
 * @brief WS2812 leds strip initilization
 *
 * @param  ws2812_handle WS2812 leds configure struct
 *
 * @return
 *     - ESP_OK success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *
 */
esp_err_t ws2812_init(ws2812_t ws2812_handle);
/**
 * @brief WS2812 leds strip deinitilization
 *
 * @param  ws2812_handle WS2812 leds configure struct
 *
 * @return
 *     - ESP_OK success
 *     - ESP_FAIL WS2812 did't init
 *
 */
esp_err_t ws2812_denit(ws2812_t ws2812_handle);
/**
 * @brief WS2812 LED set color
 *
 * @param  ws2812_handle WS2812 leds configure struct
 *
 * @param led_num LED number starting at the beginning of the LED strip
 *
 * @param red value of a red color(0-255)
 *
 * @param green value of a green color(0-255)
 *
 * @param blue value of a blue color(0-255)
 *
 * @return
 *     - ESP_OK success
 *     - ESP_FAIL WS2812 did't init
 *
 */
esp_err_t ws2812_setColor(ws2812_t ws2812_handle,int led_num, uint8_t red, uint8_t green, uint8_t blue);
/**
 * @brief WS2812 update state
 *
 * @param  ws2812_handle WS2812 leds configure struct
 *
 * @return
 *     - ESP_OK success
 *     - ESP_FAIL WS2812 did't init
 *
 */
esp_err_t ws2812_show(ws2812_t ws2812_handle);
/**
 * @brief WS2812 clear all values
 *
 * @param  ws2812_handle WS2812 leds configure struct
 *
 * @return
 *     - ESP_OK success
 *     - ESP_FAIL WS2812 did't init
 *
 */
esp_err_t ws2812_clear(ws2812_t ws2812_handle);

#endif
