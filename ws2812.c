/*
 * File: 	ws2812.c
 * Author: 	Alexander Grin, Dnipro, Ukraine
 * Emain: 	gryn2000@gmail.com
 */
#include "ws2812.h"

#include "driver/rmt.h"
#include "esp_err.h"
#include "esp_log.h"

#define LED_RMT_TX_CHANNEL	RMT_CHANNEL_0
#define BITS_PER_LED_CMD 	24

#define T0H                 14  // 0 bit high time
#define T1H                 52  // 1 bit high time
#define TL                  52  // low time for either bit

static rmt_item32_t* led_data_buffer = NULL;

esp_err_t ws2812_setColor(ws2812_t ws2812_handle,int led_num, uint8_t red, uint8_t green, uint8_t blue)
{
    if(led_data_buffer == NULL) {
        ESP_LOGE(__func__, "WS2812 didn't init!");
        return ESP_FAIL;
    }
    if(led_num >= ws2812_handle.leds_count) {
        ESP_LOGE(__func__, "WS2812 led is out of range!");
        return ESP_ERR_INVALID_ARG;
    }

    if(ws2812_handle.brightness > 100)
        ws2812_handle.brightness = 100;
    if(ws2812_handle.brightness <= 0)
        ws2812_handle.brightness = 10;

    uint32_t bits_to_send = 0;
    uint32_t mask = 1 << (BITS_PER_LED_CMD - 1);

    float tempRed = red, tempGreen = green, tempBlue = blue;
    tempRed = tempRed * ws2812_handle.brightness / 100.0;
    tempGreen = tempGreen * ws2812_handle.brightness / 100.0;
    tempBlue = tempBlue * ws2812_handle.brightness / 100.0;
    red = (uint8_t)tempRed;
    green = (uint8_t)tempGreen;
    blue = (uint8_t)tempBlue;

    if(ws2812_handle.leds_type) {
        bits_to_send = green;
        bits_to_send = bits_to_send  << 8;
        bits_to_send += red;
        bits_to_send = bits_to_send << 8;
        bits_to_send += blue;
    }
    else {
        bits_to_send = red;
        bits_to_send = bits_to_send  << 8;
        bits_to_send += green;
        bits_to_send = bits_to_send << 8;
        bits_to_send += blue;
	}

    for (uint32_t bit = 0; bit < BITS_PER_LED_CMD; bit++) {
      uint32_t bit_is_set = bits_to_send & mask;
      led_data_buffer[led_num * BITS_PER_LED_CMD + bit] = bit_is_set ?
                                                          (rmt_item32_t){{{T1H, 1, TL, 0}}}:
                                                          (rmt_item32_t){{{T0H, 1, TL, 0}}};
      mask >>= 1;
    }
    return ESP_OK;
}

esp_err_t ws2812_clear(ws2812_t ws2812_handle)
{
    if(led_data_buffer == NULL) {
        ESP_LOGE(__func__, "WS2812 didn't init!");
        return ESP_FAIL;
    }

    uint32_t bits_to_send = 0;
    uint32_t mask = 1 << (BITS_PER_LED_CMD - 1);

    for(int led = 0; led < ws2812_handle.leds_count; led++) {
        for (uint32_t bit = 0; bit < BITS_PER_LED_CMD; bit++) {
            uint32_t bit_is_set = bits_to_send & mask;
            led_data_buffer[led * BITS_PER_LED_CMD + bit] = bit_is_set ?
                                                            (rmt_item32_t){{{T1H, 1, TL, 0}}} :
                                                            (rmt_item32_t){{{T0H, 1, TL, 0}}};
	      mask >>= 1;
	    }
	}
	return ESP_OK;
}

esp_err_t ws2812_show(ws2812_t ws2812_handle)
{
    if(led_data_buffer == NULL)
    {
        ESP_LOGE(__func__, "WS2812 didn't init!");
        return ESP_FAIL;
    }

    ESP_ERROR_CHECK(rmt_write_items(LED_RMT_TX_CHANNEL, led_data_buffer, ws2812_handle.leds_count*BITS_PER_LED_CMD, false));
    ESP_ERROR_CHECK(rmt_wait_tx_done(LED_RMT_TX_CHANNEL, portMAX_DELAY));
    return ESP_OK;
};

esp_err_t ws2812_init(ws2812_t ws2812_handle)
{
    if(ws2812_handle.gpio_pin > GPIO_NUM_MAX)
    {
        ESP_LOGE(__func__, "Invalid gpio_pin argument!");
        return ESP_ERR_INVALID_ARG;
    }
    if(ws2812_handle.leds_type < 0 || ws2812_handle.leds_type > WS2812_GRB)
    {
        ESP_LOGE(__func__, "Invalid ws2812_leds_type_t argument!");
        return ESP_ERR_INVALID_ARG;
    }

    led_data_buffer = malloc(ws2812_handle.leds_count*BITS_PER_LED_CMD*sizeof(rmt_item32_t));

    rmt_config_t config;
    config.rmt_mode = RMT_MODE_TX;
    config.channel = LED_RMT_TX_CHANNEL;
    config.gpio_num = (gpio_num_t)ws2812_handle.gpio_pin;
    config.mem_block_num = 3;
    config.tx_config.loop_en = false;
    config.tx_config.carrier_en = false;
    config.tx_config.idle_output_en = true;
    config.tx_config.idle_level = 0;
    config.clk_div = 2;

    ESP_ERROR_CHECK(rmt_config(&config));
    ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));
    ws2812_clear(ws2812_handle);
    ws2812_show(ws2812_handle);
    return ESP_OK;
}

esp_err_t ws2812_denit(ws2812_t ws2812_handle)
{
    if(led_data_buffer == NULL)
    {
        ESP_LOGE(__func__, "WS2812 didn't init!");
        return ESP_FAIL;
    }
    ws2812_clear(ws2812_handle);
    ws2812_show(ws2812_handle);
    free(led_data_buffer);
    led_data_buffer = NULL;
    return ESP_OK;
}
